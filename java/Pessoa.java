package java;

public class Pessoa{
    private String nome;
	private int idade;
	private static ArrayList<Pessoa> pessoas = null;
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public void setIdade(int idade){
		this.idade = idade;
	}
	
	public void salvarNoBanco(Pessoa p){
		pessoas = pessoas == null ? new ArrayList<Pessoa>(); : pessoas;
		if(validarPessoa(p)){
		  pessoas.add(p);			
		}
	}
	
	private boolean validarPessoa(Pessoa p){
		if(p.getNome() == null) || (p.getIdade) == null){
			return false;
		}
		return true;
	}
	
}